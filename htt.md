# How to tmux

## Why

* Detach
* Multiple windows
* Spliting

## Basic usage

* man tmux
* c-b ?

### Open, close

* `tmux` - run new session
* `exit` - close the shell, closes window, last window closes tmux
* `c-d` - shortcut for `exit`
* `c-b x` - kill current pane

### Detach, attach

* `c-b d` - detach current tmux session
* kill terminal won't close the session
* `tmux ls` - list all available tmux sessions
* `tmux attach` - attach tmux session to current terminal
* `tmux a` - shortcut for attach

### Scroll

* `c-b pgup` - enter select mode and scroll up
* `c-b [ pgup/down` - same

### Multiple windows

* `c-b c` - create new window
* `c-b w` - show all windows and select interactively
* `c-b n` - go to next window
* `c-b p` - go to previous window
* `c-b [0-9]` - go to nth window
* `c-b '42` - go to window number 42

### Split

* `c-b "` - vertical split
* `c-b %` - horizontal split
* `c-b o` - go to next pane
* `c-b ;` - go to last used pane
* `c-b q` - show pane numbers
* `c-b q[0-9]` - jump to pane by number
* `c-b space` - select different pane layout
* `c-b alt-arrow` - resize pane

## Advanced usage

### Sessions

* `tmux new-session -d -s dev` - start new detached session called "dev"
* `tmux new-session -d -s ops` - start new detached session called "ops"
* `tmux new-session -d -s qa` - start new detached session called "qe"
* `tmux ls` - list available sessions
* `tmux a -t dev` - attach session called "dev"
* `c-b s` - list available sessions and select interactively
* `c-b :rename qe` - rename current session to "qe"
* `c-b $` - rename current session to "qe"
* `c-b D` - list attached session and force-detach interactively

### Screen sharing

     user1 $ tmux -S /tmp/pair
     user1 $ chmod 777 /tmp/pair
     user2 $ tmux -S /tmp/pair attach

Nice for shadowing operations and pair programming.

### tmux scripting

    #!/bin/sh
    tmux new-session -d -s dev 'vim .'
    tmux split-window -h -t dev 'make test; bash'
    tmux attach-session -d -t dev

This script will create new tmux session called "dev", launches vim in first window,
performs horizontal split and lauches unit tests and bash in the other pane.
Last, it attaches the session to current terminal.

Tmux can be controlled from outside and from inside.

## Related

* nohup - run a command immune to hangups
* disown - remove jobs from current shell
* screen - full-screen window manager that multiplexes a physical terminal
* dvtm - dynamic virtual terminal manager
* dtach - emulates the detach feature of screen
